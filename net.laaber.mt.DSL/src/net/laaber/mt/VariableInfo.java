package net.laaber.mt;

public class VariableInfo {
	
	private String name;
	private String uri;
	private String hadlId;
	private Class<?> javaType;
	private String containerUri;
	
	protected VariableInfo() {}

	public String getHadlId() {
		return hadlId;
	}

	void setHadlId(String hadlId) {
		this.hadlId = hadlId;
	}

	public Class<?> getJavaType() {
		return javaType;
	}

	void setJavaType(Class<?> javaType) {
		this.javaType = javaType;
	}

	public String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	void setUri(String uri) {
		this.uri = uri;
	}

	public String getContainerUri() {
		return containerUri;
	}

	void setContainerUri(String containerUri) {
		this.containerUri = containerUri;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((containerUri == null) ? 0 : containerUri.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableInfo other = (VariableInfo) obj;
		if (containerUri == null) {
			if (other.containerUri != null)
				return false;
		} else if (!containerUri.equals(other.containerUri))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VariableInfo [name=" + name + ", uri=" + uri + ", hadlId=" + hadlId + ", javaType=" + javaType
				+ ", containerUri=" + containerUri + "]";
	}
}
