package net.laaber.mt.jvmmodel

import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLLinkageConnector
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeModel
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeMonitor
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.Neo4JbackedRuntimeModel
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.RuntimeRegistry
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.SurrogateFactoryResolver
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabRef
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Inject
import com.google.inject.Injector
import com.google.inject.Provides
import fj.data.Either
import fj.data.Option
import java.io.File
import java.util.AbstractMap
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Singleton
import net.laaber.mt.VariableManager
import net.laaber.mt.dSL.Acquire
import net.laaber.mt.dSL.AppendableParam
import net.laaber.mt.dSL.Dereference
import net.laaber.mt.dSL.Domainmodel
import net.laaber.mt.dSL.HadlInput
import net.laaber.mt.dSL.HadlListType
import net.laaber.mt.dSL.HadlListVariableAppend
import net.laaber.mt.dSL.HadlSimpleType
import net.laaber.mt.dSL.HadlStatement
import net.laaber.mt.dSL.HadlVariableAssignment
import net.laaber.mt.dSL.HadlVariableDeclaration
import net.laaber.mt.dSL.Iteration
import net.laaber.mt.dSL.JavaInput
import net.laaber.mt.dSL.Link
import net.laaber.mt.dSL.Load
import net.laaber.mt.dSL.Output
import net.laaber.mt.dSL.Reference
import net.laaber.mt.dSL.Release
import net.laaber.mt.dSL.ResourceDescriptor
import net.laaber.mt.dSL.StartScope
import net.laaber.mt.dSL.StopScope
import net.laaber.mt.dSL.StructuralStatement
import net.laaber.mt.dSL.Task
import net.laaber.mt.dSL.Unlink
import net.laaber.mt.dsl.lib.hadl.HadlElements
import net.laaber.mt.dsl.lib.hadl.HadlLinker
import net.laaber.mt.dsl.lib.hadl.HadlLinkerImpl
import net.laaber.mt.dsl.lib.hadl.HadlModelInteractor
import net.laaber.mt.dsl.lib.hadl.HadlModelInteractorImpl
import net.laaber.mt.dsl.lib.hadl.HadlMonitor
import net.laaber.mt.dsl.lib.hadl.HadlMonitorImpl
import net.laaber.mt.dsl.lib.hadl.ProcessScope
import net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure
import org.apache.commons.io.FileUtils
import org.eclipse.emf.common.util.EList
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmFormalParameter
import org.eclipse.xtext.common.types.JvmMember
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.JvmVisibility
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor.IPostIndexingInitializing
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder
import org.neo4j.graphdb.GraphDatabaseService
import org.neo4j.graphdb.factory.GraphDatabaseFactory

import static net.laaber.mt.dsl.lib.hadl.HadlElements.*

/**
 * <p>Infers a JVM model from the source model.</p> 
 *
 * <p>The JVM model should contain all elements that would appear in the Java code 
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>     
 */
class DSLJvmModelInferrer extends AbstractModelInferrer {

    /**
     * convenience API to build and initialize JVM types and their members.
     */
	@Inject extension JvmTypesBuilder
	
	@Inject
	private HadlModelInteractor mi
	
	@Inject
	var VariableManager vm
	
	var AtomicInteger elementCounter
	var HADLmodel model = null

	/**
	 * The dispatch method {@code infer} is called for each instance of the
	 * given element's type that is contained in a resource.
	 * 
	 * @param element
	 *            the model to create one or more
	 *            {@link JvmDeclaredType declared
	 *            types} from.
	 * @param acceptor
	 *            each created
	 *            {@link JvmDeclaredType type}
	 *            without a container should be passed to the acceptor in order
	 *            get attached to the current resource. The acceptor's
	 *            {@link IJvmDeclaredTypeAcceptor#accept(org.eclipse.xtext.common.types.JvmDeclaredType)
	 *            accept(..)} method takes the constructed empty type for the
	 *            pre-indexing phase. This one is further initialized in the
	 *            indexing phase using the closure you pass to the returned
	 *            {@link IPostIndexingInitializing#initializeLater(org.eclipse.xtext.xbase.lib.Procedures.Procedure1)
	 *            initializeLater(..)}.
	 * @param isPreIndexingPhase
	 *            whether the method is called in a pre-indexing phase, i.e.
	 *            when the global index is not yet fully updated. You must not
	 *            rely on linking using the index if isPreIndexingPhase is
	 *            <code>true</code>.
	 */
   	def dispatch void infer(Domainmodel element, IJvmDeclaredTypeAcceptor acceptor, boolean isPreIndexingPhase) {
   		System.out.println("start generating")
   		if (model == null) {
	   		val loadResult = mi.load(element.file);
			if (loadResult.isLeft) {
				// error handling
				throw loadResult.left().value
			}
			model = loadResult.right().value
			
			val mtu = new ModelTypesUtil
			mtu.init(model)
			HadlElements.modelTypesUtil = mtu
   		}
   		vm.reset
   		vm.setUp(element)
   		resetCounters		
		
		// create tasks
		acceptor.accept(
			element.toClass(element.className) [
				addSetUp(element, members)
				
				// handle tasks
				for (t : element.tasks) {
					resetCounters
					val _m = members
					members += t.toMethod(t.name, returnType(t)) [
						exceptions += typeRef(Throwable)
						
						// always pass ProcessScope as first parameter
						parameters += t.toParameter("processScope", typeRef(ProcessScope))
						// handle parameters
						val hadlInputChecks = new StringBuilder
						hadlInputChecks.append("// Begin hADL Input checks\n")
						for (in : t.inputs) {
							switch in {
								JavaInput: parameters += in.toParameter(in.variable.name, in.javaType) 
								HadlInput: hadlInputChecks.append(hadlInputCheck(in, parameters))
							}
						}
						hadlInputChecks.append("// End hADL Input checks\n")
						
						// handle statements
						val outputMap = outputMap(t.outputs)
						val String b = t.statements.fold("") [s, n | s + "\n" + statement(n, _m, outputMap, true)]
						body = '''
							«addProcessScope»
							«hadlInputChecks.toString»
							final «Map.name»<String, Object> ret = new «HashMap.name»<>();
							ret.put("«VariableManager::processScopeKey»", ps);
							final «List.name»<«SurrogateStateChangeFailure.name»> «VariableManager::failureListKey» = new «ArrayList.name»<>();
							ret.put("«VariableManager::failureListKey»", failureList);
							«b»
							return ret;
						'''
					]
				}
			]
		)

		System.out.println("finished generating")
   	}
   	
   	private def void resetCounters() {
   		elementCounter = new AtomicInteger(0)
   	}
   	
   	private def String addProcessScope() {
   		return '''
   		«ProcessScope.name» ps = processScope;
   		if (ps == null) {
   			ps = setUp();
   		}
   		'''
   	}
   	
   	private def String hadlInputCheck(HadlInput in, EList<JvmFormalParameter> parameters) {
   		if (in.type == null) {
   			return ""
   		}
   		
   		val ctr = elementCounter.incrementAndGet
   		
   		val t = in.type
   		var JvmTypeReference type
   		var String ret
   		
   		// handle different types
   		switch t {
   			HadlSimpleType: {   		
		   		val typeResult = vm.operationalTypeForHadlInput(t.element)
		   		if (typeResult.isNone) {
		   			return ""
		   		}
		   		type = typeRef(typeResult.some())
		   		ret = '''
				if (!ps.getModelTypesUtil().isTypeSupertypeOf(ps.getModelTypesUtil().getById("«t.element»"), ps.getModelTypesUtil().getByTypeRef(«in.variable.name».getInstanceOf()))) {
					throw new IllegalArgumentException("«in.variable.name» is not a «t.element»");
				}
				'''
   			}
   			HadlListType: {
   				val typeResult = vm.operationalTypeForHadlInput(t.element)
		   		if (typeResult.isNone) {
		   			return ""
		   		}
		   		type = typeRef(List, typeRef(typeResult.some()))
		   		ret = '''
		   		final «THADLarchElement.name» st«ctr» = ps.getModelTypesUtil().getById("«t.element»");
		   		for (int i«ctr» = 0; i«ctr» < «in.variable.name».size(); i«ctr»++) {
		   			if (!ps.getModelTypesUtil().isTypeSupertypeOf(st«ctr», ps.getModelTypesUtil().getByTypeRef(«in.variable.name».get(i«ctr»).getInstanceOf()))) {
		   				throw new IllegalArgumentException("«in.variable.name» does not contain just elements of «t.element»");
		   			}
		   		}
		   		'''
   			}
   			default: return ""
   		}
   		
		parameters += in.toParameter(in.variable.name, type)
		ret
   	}
   	
   	def private Map<String, Either<String, String>> outputMap(EList<Output> o) {
   		val om = new HashMap<String, Either<String, String>>
   		if (o == null) {
   			return om
   		}
   		for (out : o) {
   			if (out.variable != null && out.key != null) {
   				if (out.key.name != null) {
   					om.put(out.variable.name, Either.left(out.key.name))
   				} else if (out.key.keyVariable != null) {
   					om.put(out.variable.name, Either.right(out.key.keyVariable.name))
   				}
   			}
   		}
   		om
   	}
   	
   	private def void addSetUp(Domainmodel element, EList<JvmMember> members) {
		members += element.toMethod("setUp", typeRef(ProcessScope)) [
			visibility = JvmVisibility.PRIVATE
			exceptions += typeRef(Throwable)
			body = '''
				final «HadlModelInteractor.name» mi = new «HadlModelInteractorImpl.name»();
				«Either.name»<«Throwable.name», «HADLmodel.name»> loadResult = mi.load("«element.file»");
				if (loadResult.isLeft()) {
					throw loadResult.left().value();
				}
				final «HADLmodel.name» model = loadResult.right().value();
				
				final «Injector.name» baseInjector = «Guice.name».createInjector(new «AbstractModule.name»() {
					@«Override.name»
					protected void configure() {
					}
					
					@«Provides.name»
					@«Singleton.name»
					«Executable2OperationalTransformer.name» getTransformer() {
						return new «Executable2OperationalTransformer.name»();
					}
					
					@«Provides.name»
					@«Singleton.name»
					«ModelTypesUtil.name» getTypesUtil() {
						«ModelTypesUtil.name» mtu = new «ModelTypesUtil.name»();
						mtu.init(model);
						return mtu;
					}
				});
				
				final «Injector.name» injector = «Guice.name».createInjector(new «AbstractModule.name»() {
					@«Override.name»
					protected void configure() {
						bind(«HADLLinkageConnector.name».class).in(«Singleton.name».class);
						bind(«HADLruntimeMonitor.name».class).in(«Singleton.name».class);
						bind(«HadlLinker.name».class).to(«HadlLinkerImpl.name».class).in(«Singleton.name».class);
						bind(«HadlMonitor.name».class).to(«HadlMonitorImpl.name».class).in(«Singleton.name».class);
						bind(«SensorFactory.name».class).to(«element.sensorFac.type.qualifiedName».class).in(«Singleton.name».class);
						bind(«ProcessScope.name».class).in(«Singleton.name».class);
					}
					
					@«Provides.name»
					@«Singleton.name»
					«HadlModelInteractor.name» getModelInteractor() {
						return mi;
					}
					
					@«Provides.name»
					@«Singleton.name»
					«Executable2OperationalTransformer.name» getTransformer() {
						return baseInjector.getInstance(«Executable2OperationalTransformer.name».class);
					}
					
					@«Provides.name»
					@«Singleton.name»
					«RuntimeRegistry.name» getRegistry() {
						return new «RuntimeRegistry.name»();
					}
					
					@«Provides.name»
					@«Singleton.name»
					«ModelTypesUtil.name» getTypesUtil() {
						return baseInjector.getInstance(«ModelTypesUtil.name».class);
					}
					
					@«Provides.name»
					@«Singleton.name»
					«SurrogateFactoryResolver.name» getFactoryResolver() {
						«SurrogateFactoryResolver.name» sfr = new «SurrogateFactoryResolver.name»();
						try {
							sfr.resolveFactoriesFrom(model);
						} catch («FactoryResolvingException.name» e) {
							e.printStackTrace();
						}
						return sfr;
					}
					
					@«Provides.name»
					@«Singleton.name»
					«HADLruntimeModel.name» getRuntimeModel() {
						«Neo4JbackedRuntimeModel.name» hrm = new «Neo4JbackedRuntimeModel.name»();
						baseInjector.injectMembers(hrm);
						// setup Neo4J DB
						String dbPath = "neo4j-store";
						«FileUtils.name».deleteQuietly(new «File.name»(dbPath));
						«GraphDatabaseService.name» graphDb = new «GraphDatabaseFactory.name»().newEmbeddedDatabase(dbPath);
						hrm.init(graphDb, model);
						return hrm;  							
					}
				}); 
				
				«HadlLinker.name» linker = injector.getInstance(«HadlLinker.name».class);
				
				«Option.name»<«Throwable.name»> setUpError = linker.setUp(null);
				if (setUpError.isSome()) {
					throw new «RuntimeException.name»(setUpError.some().getCause());
				}
				
				return injector.getInstance(«ProcessScope.name».class);
			'''
		]
   	}
   	
   	private def JvmTypeReference returnType(Task t) {
		return typeRef(Map, typeRef(String), typeRef(Object))
   	}
   	
   	private def dispatch String statement(HadlStatement s, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		return hadlStatement(s, members, outputMap, immediateHadlFailureHandling)
   	}
   	
   	def private String handleEitherException(String either, boolean immediateHadlFailureHandling) {
   		if (either == null) {
   			return ""
   		}
   		'''
   		if («either».isLeft()) {
   			«handleException(either + ".left().value()", immediateHadlFailureHandling)»
   		}
   		'''
   	}
   	
   	def private String handleOptionException(String option, boolean immediateHadlFailureHandling) {
   		if (option == null) {
   			return ""
   		}
   		'''
   		if («option».isSome()) {
   			«handleException(option + ".some()", immediateHadlFailureHandling)»
   		}
   		'''
   	}
   	
   	def private String handleException(String e, boolean immediateHadlFailureHandling) {
   		if (e == null) {
   			return ""
   		}
   		'''
   		if («e» instanceof «SurrogateStateChangeFailure.name») {
   			«VariableManager::failureListKey».add((«SurrogateStateChangeFailure.name») «e»);
   			«IF immediateHadlFailureHandling»
   				return ret;
   			«ELSE»
   				continue;
   			«ENDIF»
   		} else {
   			throw «e»;
   		}
   		'''
   	}
   	
   	def private String handleOutput(Map<String, Either<String, String>> outputMap, String variableName) {
   		if (outputMap == null || variableName == null) {
   			return ""
   		}
   		if (outputMap.containsKey(variableName)) {
   			val key = outputMap.get(variableName)
   			if (key.isLeft) {
   				'''ret.put("«key.left().value»", «variableName»);'''
   			} else {
   				'''ret.put(«key.right().value», «variableName»);'''
   			}
   		} else {
   			""
   		}
   	}
   	
   	private def dispatch String hadlStatement(Acquire c, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (c.variable == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		val opTypeResult = vm.operationalTypeForHadlInput(c.element)
   		if (opTypeResult.isNone) {
   			return ""
   		}
   		val opType = opTypeResult.some().name
   		'''
   		// Begin Create Statement
   		final «TResourceDescriptor.name» rd«ctr» = «rd(c.rd)»;
   		final «Either.name»<Throwable, «THADLarchElement.name»> acquireResult«ctr» = ps.getLinker().acquire("«c.element»", rd«ctr»);
   		«handleEitherException("acquireResult" + ctr, immediateHadlFailureHandling)»
   		final «opType» «c.variable.name» = («opType») acquireResult«ctr».right().value();
   		«handleOutput(outputMap, c.variable.name)»
   		// End Create Statement
   		'''
   	}
   	
   	def private String rd(ResourceDescriptor r) {
   		if (r == null || r.factory == null || r.factory.type == null || r.method == null) {
   			return ""
   		}
   		'''«r.factory.type.qualifiedName».«r.method.name»(«IF r.params != null»«r.params.map[rdParam].reduce[p1, p2| p1 + ", " + p2]»«ENDIF»)'''
   	}
   	
   	def private String rdParam(AppendableParam ap) {
   		val p = ap.param
   		if (p == null) {
   			return ""
   		}
   		val pout = if (p.variable != null) {
   			// variable
   			p.variable.name
   		} else if (p.f != null) {
   			// float
   			p.f
   		} else if (p.s != null) {
   			// String
   			'''"«p.s»"'''
   		} else {
   			// int
   			String.valueOf(p.i)
   		}
   		val out = pout + if (ap.add != null) {
   			" + " + ap.add.name
   		} else {
   			""
   		}
   		out
   	}
   	
   	private def dispatch String hadlStatement(Release d, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (d.variable == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		'''
   		// Begin Release Statement
   		final «Option.name»<Throwable> releaseResult«ctr» = ps.getLinker().release(«d.variable.name»);
   		«handleOptionException("releaseResult" + ctr, immediateHadlFailureHandling)»
   		// End Release Statment
   		'''
   	}
   	
   	private def dispatch String hadlStatement(Link l, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (l.variable == null || l.collaborator == null || l.object == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		'''
   		// Begin Link Statement
   		final «Either.name»<Throwable, «TOperationalCollabLink.name»> linkResult«ctr» = ps.getLinker().link((«TCollaborator.name») «l.collaborator.name», («TOperationalObject.name») «l.object.name», "«l.link»");
   		«handleEitherException("linkResult" + ctr, immediateHadlFailureHandling)»
   		final «TOperationalCollabLink.name» «l.variable.name» = linkResult«ctr».right().value();
   		«handleOutput(outputMap, l.variable.name)»
   		// End Link Statement
   		'''	
   	}
   	
   	private def dispatch String hadlStatement(Unlink l, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (l.link == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		'''
   		// Begin Unlink Statement
   		final «Option.name»<Throwable> unlinkResult«ctr» = ps.getLinker().unlink(«l.link.name»);
   		«handleOptionException("unlinkResult" + ctr, immediateHadlFailureHandling)»
   		// End Unlink Statement
   		'''	
   	}
   	
   	private def dispatch String hadlStatement(Reference r, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (r.from == null || r.to == null || r.variable == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		
   		val fromInfo = vm.variableInfo(r.from)
   		if (fromInfo.isNone) {
   			return ""
   		}
   		
   		var Class<?> retType
   		if (TOperationalObject.isAssignableFrom(fromInfo.some().javaType)) {
   			retType = typeof(TOperationalObjectRef)
   		} else {
   			retType = typeof(TOperationalCollabRef)
   		}
   		
   		'''
   		// Begin Reference Statement
   		final «Either.name»<Throwable, «retType.name»> referenceResult«ctr» = ps.getLinker().reference(«r.from.name», «r.to.name», "«r.ref»");
   		«handleEitherException("referenceResult" + ctr, immediateHadlFailureHandling)»
   		final «retType.name» «r.variable.name» = referenceResult«ctr».right().value();
   		«handleOutput(outputMap, r.variable.name)»
   		// End Reference Statement
   		'''	
   	}
   	
   	private def dispatch String hadlStatement(Dereference d, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (d.ref == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		'''
   		// Begin Dereference Statement
   		final «Option.name»<Throwable> derefResult«ctr» = ps.getLinker().dereference(«d.ref.name»);
   		«handleOptionException("derefResult" + ctr, immediateHadlFailureHandling)»
   		// End Dereference Statement
   		'''	
   	}
   	
   	private def dispatch String hadlStatement(Load l, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		if (l.variable == null || l.startingFrom == null || l.vias == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		
   		val listInfo = vm.variableInfo(l.variable)
   		if (listInfo.isNone) {
   			return ""
   		}
   		val listType = vm.operationalTypeForHadlInput(listInfo.some().hadlId)
   		if (listType.isNone) {
   			return ""
   		}
   		
   		val vias = l.vias.map[
   			'''new «AbstractMap.name».SimpleEntry("«it.via.from»", "«it.via.via»")'''
   		]
   		'''
   		// Begin Load Statement
   		final «List.name»<«Map.name».Entry<String, String>> vias«ctr» = «Arrays.name».asList(«IF vias.size >= 1»«vias.get(0)»«FOR e : vias.drop(1)», «e»«ENDFOR»«ENDIF»);
   		final «Either.name»<Throwable, «List.name»<«THADLarchElement.name»>> loadResult«ctr» = ps.getMonitor().load(new «AbstractMap.name».SimpleEntry("«l.what.from»", "«l.what.via»"), «l.startingFrom.name», vias«ctr»);
   		«handleEitherException("loadResult" + ctr, immediateHadlFailureHandling)»
   		// convert List<THADLarchElement> to List<«listType.some().simpleName»> 
   		final «List.name»<«listType.some().name»> «l.variable.name» = new «ArrayList.name»<>();
   		for («THADLarchElement.name» i«ctr» : loadResult«ctr».right().value()) {
   			«l.variable.name».add((«listType.some().name») i«ctr»);	
   		}
   		«handleOutput(outputMap, l.variable.name)»
   		// End Load Statement
   		'''
   	}
   	
   	def private dispatch String hadlStatement(StartScope s, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		val ctr = elementCounter.incrementAndGet
   		'''
   		// Begin StopScope Statement
   		final «Option.name»<Throwable> startScopeResult«ctr» = ps.getLinker().startScope();
   		«handleOptionException("startScopeResult" + ctr, immediateHadlFailureHandling)»
   		// End StopScope Statement
   		'''
   	}
   	
   	def private dispatch String hadlStatement(StopScope s, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		val ctr = elementCounter.incrementAndGet
   		'''
   		// Begin StopScope Statement
   		final «Option.name»<Throwable> stopScopeResult«ctr» = ps.getLinker().stopScope();
   		«handleOptionException("stopScopeResult" + ctr, immediateHadlFailureHandling)»
   		// End StopScope Statement
   		'''
   	}
   	
   	private def dispatch String statement(StructuralStatement s, EList<JvmMember> members, Map<String, Either<String, String>> outputMap, boolean immediateHadlFailureHandling) {
   		structStatement(s, members, outputMap)
   	}
   	
   	def private dispatch String structStatement(Iteration i, EList<JvmMember> members, Map<String, Either<String, String>> outputMap) {
   		if (i.list == null || i.element == null) {
   			return ""
   		}
   		val ctr = elementCounter.incrementAndGet
   		val listInfo = vm.variableInfo(i.list)
   		if (listInfo.isNone) {
   			return ""
   		}
   		val loopVariableType = vm.operationalTypeForHadlInput(listInfo.some().hadlId)
   		if (loopVariableType.isNone) {
   			return ""
   		}
   		val loopVariableTypeName = loopVariableType.some().name
   		val loopVariableName = if (i.loopVariable != null) {
   			i.loopVariable.name
   		} else {
   			"i" + ctr
   		}
   		'''
   		// Begin Iteration Statement
   		for (int «loopVariableName» = 0; «loopVariableName» < «i.list.name».size(); «loopVariableName»++) {
   			final «loopVariableTypeName» «i.element.name» = «i.list.name».get(«loopVariableName»);
   			«i.statements.fold("") [s, n | s + "\n" + statement(n, members, outputMap, false)]»
   		}
   		if (!«VariableManager::failureListKey».isEmpty()) {
   			return ret;
   		}
   		// End Iteration Statement
   		'''
   	}
   	
   	def private dispatch String structStatement(HadlVariableDeclaration v, EList<JvmMember> members, Map<String, Either<String, String>> outputMap) {
   		if (v.variable == null || v.type == null) {
   			return ""
   		}
   		
   		val i = vm.variableInfo(v.variable)
   		if (i.isNone) {
   			return ""
   		}
   		
   		val info = i.some()
   		val sb = new StringBuilder
   		sb.append("// Begin HadlVariableDeclaration Statement\n")
   		if (typeof(List).isAssignableFrom(info.javaType)) {
   			val opType = vm.operationalTypeForHadlInput(info.hadlId)
   			if (opType.isNone) {
   				return ""
   			}
   			sb.append('''
   			// «info.javaType.simpleName» of hADL type «info.hadlId»
   			«info.javaType.name»<«opType.some().name»> «v.variable.name» = new «ArrayList.name»<>();
   			«handleOutput(outputMap, v.variable.name)»
   			''')
   		} else {
   			sb.append('''
   			// hADL type: «info.hadlId»
   			«info.javaType.name» «v.variable.name»;
   			''')
   		}
   		sb.append("// End HadlVariableDeclaration Statement\n")
   		sb.toString
   	}
   	
   	def private dispatch String structStatement(HadlVariableAssignment v, EList<JvmMember> members, Map<String, Either<String, String>> outputMap) {
   		if (v.left == null || v.right == null) {
   			return ""
   		}
   		'''
   		// Begin HadlVariableAssignment Statement
   		«v.left.name» = «v.right.name»;
   		«handleOutput(outputMap, v.left.name)»
   		// End HadlVariableAssignment Statement
   		'''
   	}
   	
   	def private dispatch String structStatement(HadlListVariableAppend v, EList<JvmMember> members, Map<String, Either<String, String>> outputMap) {
   		if (v.left == null || v.right == null) {
   			return ""
   		}
   		'''
   		// Begin HadlListVariableAppend Statement
   		«v.left.name».add(«v.right.name»);
   		// End HadlListVariableAppend Statement
   		'''
   	}
}

