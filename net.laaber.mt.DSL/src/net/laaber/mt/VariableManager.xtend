package net.laaber.mt

import at.ac.tuwien.dsg.hadl.schema.core.TCollabConnector
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement
import at.ac.tuwien.dsg.hadl.schema.core.THumanComponent
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabRef
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef
import fj.data.Option
import java.util.HashMap
import java.util.List
import net.laaber.mt.dSL.Acquire
import net.laaber.mt.dSL.Domainmodel
import net.laaber.mt.dSL.HadlInput
import net.laaber.mt.dSL.HadlListType
import net.laaber.mt.dSL.HadlSimpleType
import net.laaber.mt.dSL.HadlVariableDeclaration
import net.laaber.mt.dSL.Iteration
import net.laaber.mt.dSL.JavaInput
import net.laaber.mt.dSL.Link
import net.laaber.mt.dSL.Load
import net.laaber.mt.dSL.Output
import net.laaber.mt.dSL.Reference
import net.laaber.mt.dSL.Task
import net.laaber.mt.dSL.Var
import net.laaber.mt.dsl.lib.hadl.HadlElements
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.EcoreUtil2

class VariableManager {
	
	val public static processScopeKey = "processScope"
	val public static failureListKey = "failureList"
	
	// keys are name + task id
	// values are id of object 
	val variableNames = new HashMap<String, String>
	// keys are name + task id
	val variableToType = new HashMap<String, Class<?>>
	// keys are name + task id
	// values are hadl model ids
	val hadlVariable = new HashMap<String, String>
	val hadlListVariables = new HashMap<String, String>
	
	val outputKeys = new HashMap<String, String>
	
	private var setUp = false
	
	new() {
	}	
	
	def void setUp(Domainmodel model) {
		if (setUp) {
			return
		}
		
		for (Task t : model.tasks) {
			for (o : t.eContents) {
				setUp(o)
			}
		}

//		setUp = true
	}
	
	def private void setUp(Iteration i) {
		// add loop variable
		val listInfo = variableInfo(i.list)
		if (listInfo.isNone) {
			System.out.println("setUp Iteration error: no variable info for list")
			return
		}
		val loopVarType = operationalTypeForHadlInput(listInfo.some().hadlId)
		if (loopVarType.isNone) {
			System.out.println("setUp Iteration error: no type for hadlId (" + listInfo.some() + ")")
			return
		}
		addVariable(i.element, loopVarType.some(), listInfo.some().hadlId)
		
		if (i.loopVariable != null) {
			addVariable(i.loopVariable, typeof(int), null)
		}
		
		i.statements.forEach[ setUp(it) ]
	}
	
	def private void setUp(EObject o) {
		switch o {
			JavaInput: {
				if (!"void".equals(o.javaType.qualifiedName)) {
					val type = Class.forName(o.javaType.qualifiedName)
					if (TOperationalComponent.equals(type) ||
						TOperationalConnector.equals(type) ||
						TOperationalObject.equals(type) ||
						TOperationalCollabLink.equals(type) ||
						TOperationalCollabRef.equals(type) ||
						TOperationalObjectRef.equals(type)) {
							addVariable(o.variable, type, null)
						}
				}
			}
			HadlInput: {
				val t = o.type
				if (t == null) {
					return
				}
				switch t {
					HadlSimpleType: addHadlVariable(o.variable, t.element)
					HadlListType: addVariable(o.variable, typeof(List), t.element)
				}
			} 
			Acquire: addHadlVariable(o.variable, o.element)
			Link: addHadlVariable(o.variable, o.link)
			Reference: addHadlVariable(o.variable, o.ref)
			Load: {
				if (o.what == null) {
					return
				}
				addVariable(o.variable, typeof(List), o.what.from)
			}
			Iteration: setUp(o)
			HadlVariableDeclaration: {
				val t = o.type
				if (t == null) {
					return
				}
				switch t {
					HadlSimpleType: addHadlVariable(o.variable, t.element)
					HadlListType: addVariable(o.variable, typeof(List), t.element)
				}
			}
		}
	}
	
	def void reset() {
		variableNames.clear
		variableToType.clear
		hadlVariable.clear
		hadlListVariables.clear
		outputKeys.put(processScopeKey, processScopeKey)
		outputKeys.put(failureListKey, failureListKey)
	}
	
	def private void addHadlVariable(Var v, String hadlId) {
		val opType = operationalTypeForHadlInput(hadlId)
		if (opType.isNone) {
			return
		}
		addVariable(v, opType.some(), hadlId)
	}
	
	def Option<Class<? extends THADLarchElement>> operationalTypeForHadlInput(String hadlId) {
		if (hadlId == null) {
			return Option.none
		}
		val el = HadlElements.modelTypesUtil.getById(hadlId)
		if (el == null) {
			return Option.none
		}
		Option.some(switch el {
			THumanComponent: TOperationalComponent
			TCollabConnector: TOperationalConnector
			TCollabObject: TOperationalObject
			TCollabLink: TOperationalCollabLink
			TCollabRef: TOperationalCollabRef
			TObjectRef: TOperationalObjectRef
			default: THADLarchElement
		})
	}
	
	def void addVariable(Var v, Class<?> type, String hadlId) {
		val keyResult = varKey(v)
		if (keyResult.isNone) {
			return
		}
		val key = keyResult.some()
		val id = EcoreUtil2::getURI(v.eContainer).toString
		variableNames.put(key, id)
		variableToType.put(key, type)
		if (hadlId != null && !hadlId.empty) {
			if (typeof(List).equals(type)) {
				hadlListVariables.put(key, hadlId)
			} else {				
				hadlVariable.put(key, hadlId)
			}
		}
	}
	
	def boolean deletable(Var v) {
		val keyResult = varKey(v)
		if (keyResult.isNone) {
			return false
		}
		val key = keyResult.some()
		val type = variableToType.get(key)
		if (type == null) {
			return false
		}
		
		if (!hadlVariable.containsKey(key)) {
			return false
		}
		return true
	}
	
	private def Option<String> varKey(Var v, EObject container) {
		if (v == null || container == null) {
			return Option.none
		}
		val id = containerId(container)
		Option.some(v.name + id)
	}
	
	private def Option<String> varKey(Var v) {
		if (v == null || v.eContainer == null || v.eContainer.eContainer == null) {
			return Option.none 
		}
		val id = if (v.eContainer instanceof Iteration) {
			containerId(v.eContainer)	
		} else {
			containerId(v.eContainer.eContainer)
		}
		Option.some(v.name + id)
	}
	
	def boolean variableExists(Var v, EObject container) {
		if (v == null || container == null) {
			return false
		}
		val keyResult = varKey(v, container)
		if (keyResult.isNone) {
			return false
		}
		val key = keyResult.some()
		val id = EcoreUtil2::getURI(v.eContainer).toString
		// check own container
		val contains = variableNames.containsKey(key)
		if (contains && !variableNames.get(key).equals(id)) {
			return true
		}
		var c = container
		// check enclosing containers
		while (!(c instanceof Task)) {
			c = c.eContainer
			val vk = varKey(v, c)
			if (vk.isNone) {
				return true
			}
			if (variableNames.containsKey(vk.some())) {
				return true
			}
		}
		false
	}
	
	private def String containerId(EObject o) {
		EcoreUtil2::getURI(o).toString
	}
	
	def boolean variableNameExists(Var v) {
		val varKey = varKey(v)
		if (varKey.isNone) {
			return false
		}
		return variableNames.containsKey(varKey.some());
	}
	
	def boolean outputKeyExists(Output o) {
		var key = o.key.name + containerId(o.eContainer)
		val id = EcoreUtil2::getURI(o).toString
		return (outputKeys.containsKey(key) && !outputKeys.get(key).equals(id))
	}
	
	def void addOutputKey(Output o) {
		val cId = containerId(o.eContainer)
		var key = o.key.name + cId
		val id = EcoreUtil2::getURI(o).toString
		outputKeys.put(key, id)
	}
	
	def Option<VariableInfo> variableInfo(Var v) {
		if (v == null) {
			return Option.none
		}
		val keyResult = varKey(v)
		if (keyResult.isNone) {
			return Option.none
		}
		val key = keyResult.some()
		val i = new VariableInfo
		// containerUri is wrong for loopVariables
		i.containerUri = containerId(v.eContainer.eContainer)
		i.javaType = variableToType.get(key)
		if (i.javaType == null) {
			return Option.none
		}
		i.name = v.name
		i.uri = variableNames.get(key)
		if (i.uri == null) {
			return Option.none
		}
		i.hadlId = if (List.isAssignableFrom(i.javaType)) {
			hadlListVariables.get(key)
		} else {
			hadlVariable.get(key)
		}
		Option.some(i)
	}
}