package net.laaber.mt

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement
import java.util.ArrayList
import java.util.List
import javax.inject.Inject
import net.laaber.mt.dSL.Acquire
import net.laaber.mt.dSL.HadlInput
import net.laaber.mt.dSL.HadlListType
import net.laaber.mt.dSL.HadlVariableDeclaration
import net.laaber.mt.dSL.Iteration
import net.laaber.mt.dSL.JavaInput
import net.laaber.mt.dSL.Link
import net.laaber.mt.dSL.Load
import net.laaber.mt.dSL.Reference
import net.laaber.mt.dSL.Task
import net.laaber.mt.dSL.Var
import net.laaber.mt.dsl.lib.hadl.HadlElements
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.EcoreUtil2

class DslUtil {
	
	val VariableManager vm
	
	@Inject
	new(VariableManager vm) {
		this.vm = vm
	}
	
	def List<Pair<String, String>> froms(Load l, boolean loadStatementComplete) {	
		val froms = new ArrayList<Pair<String, String>>
		val startingFromInfo = vm.variableInfo(l.startingFrom)
		if (startingFromInfo.isNone) {
			return froms
		}
		// always a hADL variable enforced by scope
		froms.add(startingFromInfo.some().hadlId -> "")
		
		if (l.vias == null || l.vias.size == 0) {
			return froms
		}
		
		// takeWhile handles the case when loadStatement is not complete and next statement is a load statement
		val v = l.vias.takeWhile[ it.via.from != null && it.via.via != null ].map[ it.via.from -> it.via.via ]
		if (loadStatementComplete) {
			froms.addAll(v)
		} else {
			froms.addAll(v.take(l.vias.size - 1))
		}
		froms
	}
	
	def List<Var> javaVarsByTypes(List<Class<?>> types, EObject o, EReference r) {
		val vars = new ArrayList<Var>
		if(o == null || o.eContainer == null || types == null || types.isEmpty) {
			return vars
		}
		
		var container = o
		do {
			container = container.eContainer
		} while (!(container instanceof Task))
		
		container.eContents.takeWhile[ !EcoreUtil2.hasSameURI(it, o) ].filter[
			switch it {
				JavaInput: {
					if (it.javaType == null) {
						return false
					}
					val t = try {
						Class.forName(it.javaType.qualifiedName)
					} catch (Throwable t) {
						null
					}
					if (t == null) {
						return false
					}
					types.contains(t)
				}
				Iteration: {
					if (it.loopVariable == null) {
						return false
					}
					val i = vm.variableInfo(it.loopVariable)
					if (i.isNone) {
						return false
					}
					types.contains(i.some().javaType)
 				}
				default: false
			}
		].map[
			switch it {
				JavaInput: it.variable
				Iteration: it.loopVariable
			}
		].toList
	}
	
	/**
	 * DEPRECATED: use hadlSimpleVars instead
	 * 
	 * hadlVarsByType returns the list of variables with type e that are in scope.
	 * What is in scope depends on o. 
	 * If o is within an 'iteration' also the surrounding variables are visible.
	 * If o is at base level 'task' only the variables at this level are returned
	 * 
	 * @param e the desired type of returned variables
	 * @param o the element for which the vars should be returned (e.g. to create a scope)
	 * @param r reference to the element in o
	 * @return list of variables of type e in scope
	 * 
	 */
	@Deprecated
	def <T extends THADLarchElement> List<Var> hadlVarsByJavaType(Class<T> e, EObject o, EReference r) {
		val vars = new ArrayList<Var>
		if (o == null || o.eContainer == null) {
			System.out.println("hadlVarsByType: o or o.eContainer is null")
			return vars
		}
		
		var container = o
		val excludes = new ArrayList<Class<?>>
		//TODO: add some
		do {
			container = container.eContainer
			vars.addAll(hadlVarsByTypeAndContainer(e, null, o, container, r, excludes))
		} while (!(container instanceof Task))
		vars
	}
	
	def <T extends THADLarchElement> List<Var> hadlSimpleVars(Class<T> type, String hadlId, EObject o, EReference r, List<Class<?>> excludes) {
		val vars = new ArrayList<Var>
		if (o == null || o.eContainer == null) {
			System.out.println("hadlVarsByType: o or o.eContainer is null")
			return vars
		}
		
		var container = o
		do {
			container = container.eContainer
			vars.addAll(hadlVarsByTypeAndContainer(type, hadlId, o, container, r, excludes))
		} while (!(container instanceof Task))
		vars
	}
	
	def List<Var> assignableHadlVars(EObject o, EReference r, String hadlId, Class<?> javaType) {
		val vars = assignableHadlVars(o, r)
		if (hadlId == null && javaType == null) {
			// return all assignable hadl vars
			return vars
		}
		
		vars.filter[
			val info = vm.variableInfo(it)
			if (info.isNone) {
				return false
			}
			
			var a = true
			if (hadlId != null) {
				a = a && varOfHadlType(info.some().hadlId, hadlId)
			}
			if (javaType != null) {
				a = a && info.some().javaType.isAssignableFrom(javaType)
			}
			a
		].toList
	}
	
	def private List<Var> assignableHadlVars(EObject o, EReference r) {
		val vars = new ArrayList<Var>
		if (o == null || o.eContainer == null) {
			System.out.println("hadlVarsByType: o or o.eContainer is null")
			return vars
		}
		var container = o
		do {
			container = container.eContainer
			vars.addAll(container.eContents.takeWhile[ !EcoreUtil2.hasSameURI(it, o) ].filter[
				switch it {
					HadlVariableDeclaration: true
					default: false
				}
			].map[
				switch it {
					HadlVariableDeclaration: it.variable
				}
			].toList)
		} while (!(container instanceof Task))
		vars
	}
	
	def private boolean varOfHadlType(String potentialSuperType, String candidate) {
		if (potentialSuperType == null || candidate == null) {
			return false
		}
		val mtu = HadlElements.modelTypesUtil
		val st = mtu.getById(potentialSuperType)
		val ct = mtu.getById(candidate)
		if (st == null || ct == null) {
			return false
		}
		mtu.isTypeSupertypeOf(st, ct) 
	}
	
	def private <T extends THADLarchElement> List<Var> hadlVarsByTypeAndContainer(Class<T> e, String hadlId, EObject exclude, EObject container, EReference r, List<Class<?>> eObjectExcludes) {
		val excludes = if (eObjectExcludes == null) {
			new ArrayList
		} else {
			eObjectExcludes
		}
		//TODO: check for incomplete elements, for now dirty try catch trick
		try {
			container.eContents.takeWhile[ !EcoreUtil2.hasSameURI(it, exclude) ].filter[
				switch it {
					HadlInput: {
						if (excludes.contains(typeof(HadlInput))) {
							return false
						}
						val i = vm.variableInfo(it.variable)
						if (i.isNone) {
							return false
						}
						if (typeof(List).isAssignableFrom(i.some().javaType)) {
							// only include non list types
							return false
						}
						javaAndHadlTypeCheck(e, hadlId, i.some())
					}
					Acquire: {
						if (excludes.contains(typeof(Acquire))) {
							return false
						}
						val i = vm.variableInfo(it.variable)
						if (i.isNone) {
							return false
						}
						javaAndHadlTypeCheck(e, hadlId, i.some())
					}
					Link: {
						if (excludes.contains(typeof(Link))) {
							return false
						}
						val i = vm.variableInfo(it.variable)
						if (i.isNone) {
							return false
						}
						javaAndHadlTypeCheck(e, hadlId, i.some())
					}
					Reference: {
						if (excludes.contains(typeof(Reference))) {
							return false
						}
						val i = vm.variableInfo(it.variable)
						if (i.isNone) {
							return false
						}
						javaAndHadlTypeCheck(e, hadlId, i.some())
					}
					Var: {
						if (excludes.contains(typeof(Var))) {
							return false
						}
						// only consider Vars in iteration
						if (!(it.eContainer instanceof Iteration)) {
							return false
						}
						val i = vm.variableInfo(it)
						if (i.none || i.some().hadlId == null) {
							return false
						}
						javaAndHadlTypeCheck(e, hadlId, i.some())
					}
					HadlVariableDeclaration: {
						if (excludes.contains(typeof(HadlVariableDeclaration))) {
							return false
						}
						val i = vm.variableInfo(it.variable)
						if (i.isNone) {
							return false
						}
						if (!typeof(List).isAssignableFrom(i.some().javaType)) {
							// exclude list types
							return false
						}
						javaAndHadlTypeCheck(e, hadlId, i.some())
					}
					default: false
				}
			].map[
				switch it {
					HadlInput: it.variable
					Acquire: it.variable
					Link: it.variable
					Reference: it.variable
					Var: it
					HadlVariableDeclaration: it.variable
				}
			].toList
		} catch (Throwable t) {
			System.out.println("hadlVarsByTypeAndContainer Error: " + t.message)
			t.printStackTrace
			new ArrayList<Var>
		}
	}
	
	def private javaAndHadlTypeCheck(Class<?> type, String hadlId, VariableInfo i) {
		if (type == null && hadlId == null) {
			return true
		}
		var ret = true
		if (type != null) {
			if (i.javaType == null) {
				return false
			}
			ret = ret && type.isAssignableFrom(i.javaType)
		}
		if (hadlId != null) {
			ret = ret && varOfHadlType(hadlId, i.hadlId)
		}
		ret
	}

	def List<Var> hadlListVars(String hadlId, EObject o, EReference r, List<Class<?>> excludes) {
		val vars = new ArrayList<Var>
		if (o == null || o.eContainer == null) {
			System.out.println("hadlListVars: o or o.eContainer is null")
			return vars
		}
		
		val e = if (excludes == null) {
			new ArrayList<Class<?>>
		} else {
			excludes
		}
		
		var EObject container = o
		do {
			val oldContainer = container
			container = container.eContainer
			vars.addAll(container.eContents.takeWhile[ !EcoreUtil2.hasSameURI(it, oldContainer) ].filter[
				switch it {
					HadlInput: {
						if (e.contains(typeof(HadlInput))) {
							return false
						}
						if (!(it.type instanceof HadlListType)) {
							return false
						}
						val varInfo = vm.variableInfo(it.variable)
						if (varInfo.isNone) {
							return false
						}
						return javaAndHadlTypeCheck(null, hadlId, varInfo.some())
					}
					Load: {
						if (e.contains(typeof(Load))) {
							return false
						}
						if (it.variable == null) {
							return false
						}
						val varInfo = vm.variableInfo(it.variable)
						if (varInfo.isNone) {
							return false
						}
						javaAndHadlTypeCheck(null, hadlId, varInfo.some())
					}
					HadlVariableDeclaration: {
						if (e.contains(typeof(HadlVariableDeclaration))) {
							return false
						}
						if (it.variable == null) {
							return false
						}
						val i = vm.variableInfo(it.variable)
						if (i.isNone) {
							return false
						}
						javaAndHadlTypeCheck(typeof(List), hadlId, i.some())
					}
					default: false
				}
			].map[ switch it {
				HadlInput: it.variable
				Load: it.variable
				HadlVariableDeclaration: it.variable
			} ])
		} while (!(container instanceof Task))
		vars
	}
}