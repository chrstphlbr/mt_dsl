# DSL for hADL

This repository specifies a DSL for creating valid hADL instances. This DSL is the outcome of Christoph Laaber's [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis).

## Install DSL
 1. Download Eclipse and install the [Xtext plug-in](https://eclipse.org/Xtext/download.html). It was tested with Eclipse version 4.5.2 and Xtext version 2.9.2. IntelliJ was not tested.
 2. Import -> General/Existing Projects into Workspace -> select all projects from this repository
 3. Right click `net.laaber.mt.DSL/src/net.laaber.mt.DSL.xtext` -> Run As -> Generate Xtext Artifacts
 4. Confirm ANTLR download in Eclipse console

After those steps there should not be any errors left. If for some reason this is not the case, try refreshing or restarting Eclipse. 

## Create DSL script
 1. Assume DSL is installed
 2. Right click on project `net.laaber.mt.DSL` -> Run As -> Eclipse Application (a new Eclipse window starts)
 3. Create new Java Project
 4. Set Java Build Path: right click on new project -> Properties -> Java Build Path -> Libraries -> Add External JARs -> select all libraries in `net.laaber.mt.DSL.lib/libs`
 5. Create new File in src folder with extension .col

## Dependencies
All dependencies necessary for the DSL implementation and the creation of DSL scripts are in the `net.laaber.mt.DSL.lib/libs` folder. Besides general dependencies it contains hADL related dependencies:

 * [hADL schema and runtime](https://bitbucket.org/christophdorn/hadl)
 * [hADL Synchronous Library](https://bitbucket.org/chrstphlbr/mt_dsl-lib)
 * [Sensor Factory](https://bitbucket.org/chrstphlbr/mt_evaluation) for Agilefant, Bitbucket and HipChat
 * Surrogates and Sensors for [Agilefant](https://bitbucket.org/chrstphlbr/mt_agilefant4hadl), [Bitbucket and HipChat](https://bitbucket.org/chrstphlbr/mt_atlassian4hadl)

If dependencies must be changed (especially hADL or hADL Synchronous Library), update the respective library in the folder.

## Try Out
The DSL is evaluated in Christoph Laaber's master thesis. The project [MT_Evaluation](https://bitbucket.org/chrstphlbr/mt_evaluation) provides a basic evaluation scenario for the DSL.

For more information about the DSL see Christoph Laaber's master thesis.